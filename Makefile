# Makefile for gg
# You must have the Go compiler and tools installed to build this software.

VERS=$(shell sed <gg.go -n -e '/version string/s/.*"\([^"]*\)"/\1/p')

gg: gg.go
	go build

clean:
	go clean
	rm -f *.html *.1

install: gg
	go install

get:
	go get -u ./...	# go get -u=patch for patch releases

check:
	@./ggtest | ./tapview
	@golint -set_exit_status ./...
	@go test ./...
	@-shellcheck -f gcc ggtest

fmt:
	gofmt -w .

gofmt:
	gofmt -s -d gg.go | patch -p0

SOURCES = README.adoc COPYING NEWS.adoc control gg.go \
		gg.adoc prospectus.adoc data-design.adoc ggtest \
		Makefile

.SUFFIXES: .html .adoc .1

# Requires asciidoc and xsltproc/docbook stylesheets.
.adoc.1:
	a2x --doctype manpage --format manpage $<
.adoc.html:
	asciidoc $<

version:
	@echo $(VERS)

gg-$(VERS).tar.gz: $(SOURCES) gg.1
	mkdir gg-$(VERS)
	cp $(SOURCES) gg-$(VERS)
	tar -czf gg-$(VERS).tar.gz gg-$(VERS)
	rm -fr gg-$(VERS)
	ls -l gg-$(VERS).tar.gz

release: gg-$(VERS).tar.gz gg.html hacking.html
	shipper version=$(VERS) | sh -e -x

refresh: gg.html
	shipper -N -w version=$(VERS) | sh -e -x
