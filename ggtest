#! /bin/sh

# Test suite for the Git operations used ny the back end.
# By testing there directly we remove complications due to the
# Reports in TAP.

# Not all platforms have a 'realpath' command, so fake one up if needed
# using $PWD.
# Note: GitLab's CI environment does not seem to define $PWD, however, it
# does have 'realpath', so use of $PWD here does no harm.
unset CDPATH	# See https://bosker.wordpress.com/2012/02/12/bash-scripters-beware-of-the-cdpath/

command -v realpath >/dev/null 2>&1 ||
    realpath() { test -z "${1%%/*}" && echo "$1" || echo "$PWD/${1#./}"; }

here=$(realpath .)
trap 'rm -fr $here/foo$$' EXIT HUP INT QUIT TERM

testcount=1
exitval=0

tapcd () {
    cd "$1" >/dev/null || ( echo "not ok: $0: cd failed"; exit 1 )
}

check() {
    case $? in
	0) echo "ok ${testcount} - '$1' succeeded";;
	*) echo "not ok ${testcount} - '$1' failed"; exitval=1;;
    esac
    testcount=$((testcount + 1))
}


git init -q foo$$
tapcd foo$$

touch emptyfile
git add emptyfile
test -f emptyfile
check "empty file creation"

# Create a first commit - git commit allows this to have no content
git commit -q -m "First commit succeeded"
check "First commit"
sleep 1

# Check that --allow-empty permits contentless commits after the first
git commit -q --allow-empty -m "Second commit succeeded"
check "Second commit"

# Log should have exactly two commit lines
# shellcheck disable=SC2046
test $(git log | grep -c '^commit') -eq 2
check "Log should have two commit lines"

# Doing two file adds in an empty file should produce no output
touch foo
git add foo >addout 2>&1
git add foo >>addout 2>&1
test ! -s addout
check "Two adds on empty file succeed with no output"

echo "1..$((testcount-1))"

exit $exitval

