// SPDX-License-Identifier: BSD-2-Clause
package main

import (
	"bufio"
	"bytes"
	"errors"
	"flag"
	"fmt"
	"io"
	"os"
	"os/exec"
	"strings"
	"time"

	shlex "github.com/anmitsu/go-shlex"
)

var verbose uint

func whoami() (string, string, error) {
	// Git version-control system
	out1, err1 := exec.Command("git", "config", "user.name").CombinedOutput()
	out2, err2 := exec.Command("git", "config", "user.email").CombinedOutput()
	if err1 == nil && len(out1) != 0 && err2 == nil && len(out2) != 0 {
		return strings.Trim(string(out1), "\n"), strings.Trim(string(out2), "\n"), nil
	}

	return "", "", errors.New("ill-formed whoami information")
}

// rfc3339 makes a UTC RFC3339 string from a system timestamp.
func rfc3339(t time.Time) string {
	return t.UTC().Format(time.RFC3339)
}

// armor safes its argument so it can be passed to the shell without risk
func armor(s string) string {
	return "'" + strings.Replace(s, "'", "''", -1) + "'"
}

const (
	logSHOUT    uint = 1 << iota // Errors and urgent messages
	logWARN                      // Exceptional condition, probably not bug (default log level)
	logCOMMANDS                  // Show commands as they are executed
)

// logEnable is a hook to set up log-message filtering.
func logEnable(level uint) bool {
	return verbose >= level
}

func speak(msg string, args ...interface{}) {
	fmt.Printf("gg: "+msg+"\n", args...)
}

func croak(msg string, args ...interface{}) {
	fmt.Printf(msg, args...)
	os.Exit(1)
}

/* Bottom layer: manage slave processes */

func readFromProcess(command string) (io.ReadCloser, *exec.Cmd, error) {
	cmd := exec.Command("sh", "-c", command+" 2>&1")
	cmd.Stdin = os.Stdin
	cmd.Stderr = os.Stderr
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return nil, nil, err
	}
	if logEnable(logCOMMANDS) {
		croak("%s: reading from '%s'\n",
			rfc3339(time.Now()), command)
	}
	err = cmd.Start()
	if err != nil {
		return nil, nil, err
	}
	// Pass back cmd so we can call Wait on it and get the error status.
	return stdout, cmd, err
}

func lineByLine(command string, errfmt string, hook func(string) error) error {
	stdout, cmd, err1 := readFromProcess(command)
	if err1 != nil {
		return err1
	}
	defer stdout.Close()
	r := bufio.NewReader(stdout)
	for {
		line, err2 := r.ReadString(byte('\n'))
		if err2 == io.EOF {
			if cmd != nil {
				cmd.Wait()
			}
			break
		} else if err2 != nil {
			return fmt.Errorf(errfmt, err2)
		}
		hook(line)
	}
	return nil
}

func writeToProcess(command string) (io.WriteCloser, *exec.Cmd, error) {
	cmd := exec.Command("sh", "-c", command+" 2>&1")
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	stdout, err := cmd.StdinPipe()
	if err != nil {
		return nil, nil, err
	}
	if logEnable(logCOMMANDS) {
		speak("%s: writing to '%s'\n",
			rfc3339(time.Now()), command)
	}
	err = cmd.Start()
	if err != nil {
		return nil, nil, err
	}
	// Pass back cmd so we can call Wait on it and get the error status.
	return stdout, cmd, err
}

func runProcess(dcmd string, legend string) error {
	if legend != "" {
		legend = " " + legend
	}
	if logEnable(logCOMMANDS) {
		speak("executing '%s'%s", dcmd, legend)
	}
	words, err := shlex.Split(dcmd, true)
	if err != nil {
		return fmt.Errorf("preparing %q for execution: %v", dcmd, err)
	}
	cmd := exec.Command(words[0], words[1:]...)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err = cmd.Run()
	if err != nil {
		return fmt.Errorf("executing %q: %v", dcmd, err)
	}
	return nil
}

/* Middle layer: communicate wuth the current repository */

// VCS encapsulates generation commands to the underlying VCS
type VCS struct {
	verbose     uint
	assetbranch string
}

func exists(pathname string) bool {
	_, err := os.Stat(pathname)
	return !os.IsNotExist(err)
}

func isdir(pathname string) bool {
	st, err := os.Stat(pathname)
	return err == nil && st.Mode().IsDir()
}

func newVCS(verbose uint) *VCS {
	var out VCS
	out.assetbranch = "assets"
	out.verbose = verbose
	if !exists(".git") || !isdir(".git") {
		return nil
		//croak("current directory doesn't look like a Git repository.")
	}
	return &out
}

// Get the current branch, returning empty string if we can't do it
func (vcs *VCS) currentBranch() string {
	var stash string
	lineByLine("git branch", "currentBranch: %s", func(line string) error {
		if strings.HasPrefix("* ", line) {
			stash = strings.TrimSpace(line[2:])
		}
		return nil
	})
	return stash
}

// Are we in a repository with a specified branch?
func (vcs *VCS) hasBranch(name string) bool {
	var found bool
	lineByLine("git branch", "hasBranch: %s", func(line string) error {
		if strings.TrimSpace(line) == name {
			found = true
		}
		return nil
	})
	return found
}

func (vcs *VCS) makeBranch(name string) error {
	err := runProcess("git branch "+name, "git branch creation")
	if err != nil {
		err = fmt.Errorf("makeBranch: %w", err)
	}
	return err
}

func (vcs *VCS) switchBranch(branch string) error {
	vopt := " "
	if vcs.verbose == 0 {
		vopt = " -q"
	} else if vcs.verbose == 1 {
		vopt = " "
	} else {
		vopt = "-v"
	}
	return runProcess(fmt.Sprintf("git checkout%s %s", vopt, branch), "switchBranch")
}

func (vcs *VCS) add(path string) error {
	_, err := os.Stat(path)
	if os.IsNotExist(err) {
		file, err := os.Create(path)
		if err != nil {
			return err
		}
		file.Close()
	} else {
		return fmt.Errorf("adding existing file %s", path)
	}
	// FIXME: Recognize and deal with errors due to ID collisions
	return runProcess(fmt.Sprintf("git add %s", path), "file add in repository")
}

func (vcs *VCS) commit(path string, comment string) error {
	vopt := " "
	if vcs.verbose == 0 {
		vopt = " -q"
	} else if vcs.verbose == 1 {
		vopt = " "
	} else {
		vopt = "-v"
	}
	err := runProcess(fmt.Sprintf("git commit%s --allow-empty -m %s -- %s", vopt, armor(comment), path), "committing")
	if err != nil {
		return fmt.Errorf("committing to %s", path)
	}
	return err
}

func (vcs *VCS) getLog(path string) (string, error) {
	command := "git log"
	if path != "" {
		command += " -- " + path
	}
	stdout, cmd, err1 := readFromProcess(command)
	if err1 != nil {
		return "", err1
	}
	defer stdout.Close()
	r := bufio.NewReader(stdout)
	var out string
	for {
		line, err2 := r.ReadString(byte('\n'))
		if err2 == io.EOF {
			if cmd != nil {
				cmd.Wait()
			}
			break
		} else if err2 != nil {
			return out, fmt.Errorf("getLog: %s", err2)
		}
		out += line
	}
	return out, nil
}

/* Top layer: gg command logic */

// StreamSection is a section of a dump stream interpreted as an RFC-2822-like header
type StreamSection []byte

// Extract content of a specified header field, nil if it doesn't exist
func (ss StreamSection) payload(hd string) []byte {
	offs := bytes.Index(ss, []byte(hd+": "))
	if offs == -1 {
		return nil
	}
	offs += len(hd) + 2
	end := bytes.Index(ss[offs:], []byte("\n"))
	return ss[offs : offs+end]
}

// Mutate a specified header through a hook
func (ss *StreamSection) replaceHook(htype string, hook func(string, []byte) []byte) (StreamSection, []byte, []byte) {
	header := []byte(*ss)
	offs := bytes.Index(header, []byte(htype+": "))
	if offs == -1 {
		return StreamSection(header), nil, nil
	}
	offs += len(htype) + 2
	endoffs := offs + bytes.Index(header[offs:], []byte("\n"))
	before := header[:offs]
	pathline := header[offs:endoffs]
	dup := string(pathline)
	after := make([]byte, len(header)-endoffs)
	copy(after, header[endoffs:])
	newpathline := hook(htype, pathline)
	header = before
	header = append(header, newpathline...)
	header = append(header, after...)
	return StreamSection(header), newpathline, []byte(dup)
}

// Find the index of the content of a specified field
func (ss StreamSection) index(field string) int {
	return bytes.Index([]byte(ss), []byte(field))
}

// delete - method to delete a specified header
func (ss StreamSection) delete(htype string) StreamSection {
	offs := ss.index(htype)
	if offs == -1 {
		return ss
	}
	header := []byte(ss)
	header = append(header[:offs], header[offs+bytes.Index(header[offs:], []byte("\n"))+1:]...)
	return StreamSection(header)
}

func (ss StreamSection) clone() StreamSection {
	tmp := make([]byte, len(ss))
	copy(tmp, ss)
	return tmp
}

// Engine is a command interpreter primitves for the storage manager
type Engine struct {
	vcs *VCS
}

const (
	threadALL   uint = 1 << iota // Errors and urgent messages
	threadISSUE                  // Exceptional condition, probably not bug (default log level)
	threadMR                     // Show commands as they are executed
)

var ttype uint = threadISSUE

func newEngine(verbose uint) *Engine {
	var engine Engine
	engine.vcs = newVCS(verbose)
	return &engine
}

func (engine *Engine) commit(in io.Reader) []string {
	// FIXME: unimplemented
	croak("commit is not yet implemented")
	return nil
}

func (engine *Engine) cat(threadID string, out io.Writer) []string {
	// FIXME: unimplemented
	croak("cat is not yet implemented")
	return nil
}

func (engine *Engine) list(ttype uint) []string {
	// FIXME: unimplemented
	croak("list is not yet implemented")
	return nil
}

/* The main event */

func browse(ttype uint) {
	croak("browse is not yet implemented")
}

func main() {
	var forceAll bool
	var forceIssue bool
	var forceMR bool
	var nomail bool

	flag.BoolVar(&forceAll, "a", true, "select issue threads")
	flag.BoolVar(&forceIssue, "i", true, "select issue threads")
	flag.BoolVar(&forceMR, "m", false, "select merge request threas")
	flag.BoolVar(&nomail, "n", false, "select merge request threas")
	flag.UintVar(&verbose, "v", 0, "set verbosity levek")
	flag.Parse()

	if forceAll {
		ttype = threadALL
	} else if forceMR {
		ttype = threadALL
	} else if forceIssue {
		ttype = threadISSUE
	}

	if flag.NArg() == 0 {
		croak("Type 'gg help' for usage.\n")
		os.Exit(1)
	}

	command := flag.Arg(0)
	arguments := flag.Args()[1:]

	if len(arguments) == 0 {
		croak("no command verb")
	}

	engine := newEngine(verbose)
	switch command {
	case "commit":
		// FIXME: use EDITOR when stdin is a tty
		// FIXME: Implement shipping of email notifications
		for _, threadID := range engine.commit(os.Stdin) {
			os.Stdout.WriteString(threadID + "\n")
		}
	case "cat":
		if len(arguments) == 0 {
			engine.cat("", os.Stdout)
		} else {
			for _, threadID := range arguments {
				engine.cat(threadID, os.Stdout)
			}
		}
	case "list":
		for _, threadID := range engine.list(ttype) {
			os.Stdout.WriteString(threadID + "\n")
		}
	case "browse":
		browse(ttype)
	default:
		croak("unknown command verb '" + command + "'")
	}
}

// end
