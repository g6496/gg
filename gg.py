#! /usr/bin/env python3
"""
usage: gg [-i|-m] [ commit | cat {thread-ID...} | list | browse]

-i = list or modify issues (default)
-m = list or modify merge requests
"""

import sys, os, getopt, time, signal, subprocess

verbose = 0
noexec = False

verbose         = 0
DEBUG_SHOUT     = 0    # Unconditional
DEBUG_COMMANDS  = 1    # Show commands as they are executed

def errout(msg):
    sys.stderr.write("gg: " + str(msg) + " \n" + __doc__)
    sys.exit(1)

def whoami():
    "Ask Git who you are."
    (nameerr, nameout) = subprocess.getstatusoutput("git config user.name")
    (emailerr, emailout) = subprocess.getstatusoutput("git config user.email")
    if nameerr == 0 and nameout and emailerr == 0 and emailout:
        return emailout
    else:
        raise FatalError("can't identify the user.")

def rfc3339(t):
    "RFC3339 string from Unix time."
    return time.strftime("%Y-%m-%dT%H:%M:%SZ", time.gmtime(t))

class Fatal(Exception):
    "Unrecoverable error."
    def __init__(self, msg):
        Exception.__init__(self)
        self.msg = msg
        sys.stderr.write("raising fatal error: '%s'\n" % msg)

def debug_enable(level):
    "Hook for debug filtering."
    return verbose >= level
        
def announce(lvl, msg):
    if debug_enable(lvl):
        sys.stdout.write("gg: %s\n" % msg)

# FIXME: Not yet used, needs to be called in do_or_die and popen_or_die
def armor(s):
    "Armor argument so it can be passed to the shell without risk"
    return "'" + s.replace("'", "''") + "'"
        
def do_or_die(dcmd, legend=""):
    "Either execute a command or raise a fatal exception."
    if legend:
        legend = " "  + legend
    announce(DEBUG_COMMANDS, "executing '%s'%s" % (dcmd, legend))
    try:
        retcode = subprocess.call(armor(dcmd), shell=True, stderr=sys.stderr)
        if retcode < 0:
            raise Fatal("child was terminated by signal %d." % -retcode)
        elif retcode != 0:
            raise Fatal("child returned %d." % retcode)
    except (OSError, IOError) as e:
        raise Fatal("execution of %s%s failed: %s" % (dcmd, legend, e))

class popen_or_die(object):
    "Read or write from a subordinate process."
    # The immediate parameter, if True, causes the process to be
    # immediately run and the pipe to it opened, instead of waiting
    # for the context manager protocol; for this use case, some other
    # context manager (e.g., LineParse) must explicitly close this
    # object to ensure that the subprocess shuts down cleanly
    def __init__(self, command, legend="", mode="r", errhandler=None, stderrcapture=False):
        assert mode in ("r", "w")
        self.command = command
        self.legend = legend
        self.mode = mode
        self.errhandler = errhandler
        self.stdin = (subprocess.PIPE if mode == "w" else None)
        self.stdout = (subprocess.PIPE if mode == "r" else None)
        self.stderr = (subprocess.STDOUT if (stderrcapture and mode == "r") else None)
        if self.legend:
            self.legend = " "  + self.legend
        self.fp = None
    def open(self):
        if debug_enable(DEBUG_COMMANDS):
            if self.mode == "r":
                sys.stderr.write("%s: reading from '%s'%s\n" % (rfc3339(time.time()), self.command, self.legend))
            else:
                sys.stderr.write("%s: writing to '%s'%s\n" % (rfc3339(time.time()), self.command, self.legend))
        try:
            # NOTE: the I/O streams for the subprocess are always
            # bytes; this is what we want for some operations, but we
            # will need to decode to Unicode for others to work in
            # Python 3; the polystr and make_wrapper functions handle
            # this
            self.fp = subprocess.Popen(self.command, shell=True,
                                       stdin=self.stdin, stdout=self.stdout, stderr=self.stderr)
            # The Python documentation recommends using communicate()
            # to avoid deadlocks, but this doesn't allow fine control
            # over reading the data; since we are not trying to both
            # read from and write to the same process, this should be
            # OK
            return self.fp.stdout if self.mode == "r" else self.fp.stdin
        except (OSError, IOError) as oe:
            raise Fatal("execution of %s%s failed: %s" \
                                % (self.command, self.legend, oe))
    def close(self):
        if self.fp.stdin is not None:
            self.fp.stdin.close()
        if self.fp.stdout is not None:
            # This avoids a deadlock in wait() below if the OS pipe
            # buffer was filled because we didn't read all of the data
            # before exiting the context mgr (shouldn't happen but
G            # this makes sure)
            self.fp.stdout.read()
        self.fp.wait()
        if self.fp.returncode != 0:
            if self.errhandler is None or not self.errhandler(self.fp.returncode):
                raise Fatal("%s%s returned error." % (self.command, self.legend))
        self.fp = None
    def __enter__(self):
        return self.open()
    def __exit__(self, extype, value, traceback_unused):
        if extype:
            if verbose:
                raise extype(value)
            raise Fatal("fatal exception in popen_or_die.")
        self.close()
        return False

class VCS():
    "Generate VCS commands"
    def __init__(self, verbose=0):
        self.verbose = verbose
        if not os.path.exists(".git"):
            raise Fatal("current directory doesn't look like a Git repository.")
        self.assetbranch = "assets"

    def get_current_branch(self):
        "Get the current branch."
        with popen_or_die("git branch") as fp:
            for line in fp:
                line = line.decode("ASCII")
                if line.startswith("* "):
                    return line[2:].strip()
            else:
                raise Fatal("can't find current branch")
    def has_asset_branch(self):
        "Does this repository have an assets branch."
        with popen_or_die("git branch") as fp:
            for line in fp:
                if line[2:].decode("ASCII") == self.assetbranch:
                    return True
        return False
    def checkout(self, branch):
        "Check out a specified branch."
        if verbose == 0:
            vopt = " -q"
        elif verbose == 1:
            vopt = " "
        else:
            vopt = "-v"
        return "git checkout%s %s" % (vopt, branch)
    def commit(self, headers, body):
        "Add a new commit in a message thread."
        command = ""
        if not vcs.has_assets_branch():
            command += "git branch '%s' &&" % self.assetbranch
        # FIXME: header_extract is not implemented
        tid = header_extract("X-Thread-ID", headers)
        if not os.exists(new_asset):
            command += "git add '%s' &&" % tid
        command = "git commit --allow-empty -m %s '%s'" % (headers + "\n" + body, tid)
        return command[:-3]

class assetContext():
    "Context manager for doing things on the asset branch."
    def __init__(self, noexec = False):
        self.noexec = noexec
        self.deferred = []
        self.previous_handlers = {}
        self.startbranch = vcs.get_current_branch()
    def defer_signal(self, sig_num, stack_frame):
        self.deferred.append(sig_num)
    def __enter__(self):
        # Replace existing signal handlers with deferred handler
        for sig_num in (signal.SIGHUP, signal.SIGINT, signal.SIGTERM):
            # signal.signal returns None when no handler has been
            # set in Python, which is the same as the default
            # handler (SIG_DFL) being set
            self.previous_handlers[sig_num] = (
                signal.signal(sig_num, self.defer_signal) or signal.SIG_DFL)
        command = vcs.checkout(vcs.assetbranch)
        if self.noexec:
            command = "echo " + command
        do_or_die(command)
    def __exit__(self, extype, value, traceback_unused):
        if extype and verbose > 0:	# FIXME: needs to be internalize
            raise extype(value)
        command = vcs.checkout(self.startbranch)
        if self.noexec:
            command = "echo " + command
        do_or_die(command)
        # Restore handlers
        for sig_num, handler in self.previous_handlers.items():
            signal.signal(sig_num, handler)
        # Send deferred signals
        while self.deferred:
            sig_num = self.deferred.pop(0)
            os.kill(os.getpid(), sig_num)
        return True

class ggStore():
    "Encapulate the storage layer of a gg-enabled repository"
    def __init__(self):
        self.verbose = 0
        self.noexec = False

    def commit(self, stream):
        "Add new message to the thread it refers to. Crete new threads as required."
        newIDs = []
        with assetContext():
            # FIXME: block_parser is not implemented
            for (headers, body) in block_parser(stream):
                (tid, headers) = header_pop("X-Thread-ID", headers)
                # FIXME: header_pop is not implemented
                (_, headers) = header_pop("Date", headers)
                (_, headers) = reader_pop("From", headers)
                if tid is None:            
                    new_asset = atype + "/" + rfc3339(time.time()) + "!" + whoami()
                    headers = headers + "X-Thread-ID: " + new_asset + "\n"
                do_or_die(vcs.commit(headers, body), "commit thread update")
                newIDs.append(new_asset)
        return newIDs

    def cat(self, aid):
        "Dump a specified message thread."
        with assetContext():
            if not os.exists(aid):
                raise Fatal("no such thread as " + aid)
            # FIXME: This is wrong, need to grovel through commits
            with open(aid) as fp:
                return fp.read()
        raise Fatal("branch context set failed in cat")

    def list(self, atype):
        "List all asset IDs."
        idlist = []
        if vcs.has_asset_branch():
            with assetContext(self.noexec):
                try:
                    for fn in os.listdir(atype):
                        if "@" in fn:
                            idlist.append(fn)
                except FileNotFoundError:
                    raise Fatal("asset directory not found")
        return idlist

    def set_debug(self, level, noexec = False):
        "Set debug level"
        self.verbose = level
        self.noexec = noexec
    
def browse_assets(aid_list):
    "Browse assets by ID."
    errout("browse is not yet implemented (%s)" % id_list)

if __name__ == '__main__':
    # Process options
    try:
        (options, arguments) = getopt.getopt(sys.argv[1:], "inmv")
    except getopt.GetoptError as e:
        print(e)
        sys.exit(1)
    asset_type = "issue"

    for (switch, val) in options:
        if switch == '-i':
            asset_type = "issue"
        elif switch == '-m':
            asset_type = "mr"
        elif switch == '-n':
            noexec = True
        elif switch == '-v':
            verbose += 1
        else:
            sys.stder.write(__doc__)

    if len(arguments) == 0:
        errout("no command verb")
    else:
        try:
            vcs = VCS(verbose=verbose)
            assets = ggStore()
            assets.set_debug(level=verbose, noexec=noexec)

            subcommand = arguments.pop(0)
            if subcommand == 'commit':
                # FIXME: use EDITOR when stdin is a tty
                for thread_id in assets.commit(sys.stdin):
                    sys.stdout.write(thread_id + "\n")
            elif subcommand == 'cat':
                if len(arguments) != 1:
                    errout("cat requires a single thread-ID argument")
                else:
                    assets.cat(arguments[0])
            elif subcommand == 'list':
                if len(arguments) != 0:
                    errout("list accepts no arguments")
                for aid in assets.list(asset_type):
                    sys.stdout.write(aid + "\n")
            elif subcommand == 'browse':
                if len(arguments) != 0:
                    errout("browse accepts no arguments")
                browse_assets(assets.list(asset_type))
        except Fatal as xe:
            errout(xe.msg)

# end
