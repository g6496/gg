package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"testing"
	"time"
)

// To do: Tesrs for lineByLine, writeToProcess, runProcess

func assertBool(t *testing.T, see bool, expect bool) {
	t.Helper()
	if see != expect {
		t.Errorf("assertBool: expected %v saw %v", expect, see)
	}
}

func assertTrue(t *testing.T, see bool) {
	t.Helper()
	assertBool(t, see, true)
}

func assertStringEqual(t *testing.T, a string, b string) {
	t.Helper()
	if a != b {
		t.Fatalf("assertEqual: expected %q == %q", a, b)
	}
}

func assertIntEqual(t *testing.T, a int, b int) {
	t.Helper()
	if a != b {
		t.Errorf("assertIntEqual: expected %d == %d", a, b)
	}
}

func TestArmor(t *testing.T) {
	type testEntry struct {
		before string
		after  string
	}
	tests := []testEntry{
		{"aaa", "'aaa'"},
		{"b'c", "'b''c'"},
		{"e''f", "'e''''f'"},
	}
	for _, item := range tests {
		assertStringEqual(t, armor(item.before), item.after)
	}
}

func TestReadProcess(t *testing.T) {
	ofp, cmd, err1 := readFromProcess("echo 'Hello, world!'")
	if err1 != nil {
		t.Fail()
	}
	defer ofp.Close()
	r := bufio.NewReader(ofp)
	line, _ := r.ReadString(byte('\n'))
	cmd.Wait()
	assertStringEqual(t, line, "Hello, world!\n")
}

func TestVCS(t *testing.T) {
	here, err := os.Getwd()
	if err != nil {
		t.Errorf("Getwd: %w", err)
	}
	reponame1 := fmt.Sprintf("alpha-%s%d", rfc3339(time.Now()), os.Getpid())
	defer func() {
		os.Chdir(here)
		if err := os.RemoveAll(reponame1); err != nil {
			t.Errorf("directory cleanup: %s", err)
		}
	}()

	err = runProcess("git init -q "+reponame1, "repository initialization")
	if err != nil {
		t.Errorf("test repository creation: %s", err)
	}

	err = os.Chdir(reponame1)
	if err != nil {
		t.Errorf("repository chdir: %s", err)
	}
	repo1 := newVCS(0)
	if repo1 == nil {
		t.Errorf("%s is unexpectely not a directory", reponame1)
	}
	testbranch1 := "foobar"
	if repo1.hasBranch(testbranch1) {
		t.Errorf("Within %s, hasBranchname(%s) failed on first call", reponame1, testbranch1)
	}
	if err = repo1.add("README"); err != nil {
		t.Errorf("Within %s, anchor file addition failed: %s", reponame1, err)
	}
	breadcrumb := "This is a sample commit.\n"
	if err = repo1.commit("README", breadcrumb); err != nil {
		t.Errorf("Within %s, anchor file addition failed: %s", reponame1, err)
	}
	logstr, err := repo1.getLog("")
	if !strings.Contains(logstr, breadcrumb) {
		t.Errorf("Within %s, logg fetch failed: %s", reponame1, err)
	}
	err = repo1.makeBranch(testbranch1)
	if err != nil {
		t.Errorf("Within %s, makeBranchname(%s) failed with %w", reponame1, testbranch1, err)
	}
	if !repo1.hasBranch(testbranch1) {
		t.Errorf("Within %s, hasBranchname(%s) failed on second call", reponame1, testbranch1)
	}
}

// end
